find_max_double = function() {
  val = 1.0
  ind = 1:1
  while (! is.infinite(val)) {
    prev = val
    val = val * 2
    ind = ind + 1:1
#    if ((ind %% 100) == 0) {
#      cat(ind, val, prev, '\n')
#    }
  }
  
  cat('1st limit', ind, prev, '\n')
  
  start = val = prev
  ind = 1:1
  delta = val / 2
  cat('First delta: ', delta, '\n')
  while(delta != 1) {
    prev = val
    
    val = val + delta
    if (is.infinite(val)) {
      cat(ind, val, delta, prev, (prev - start),'Fail\n')
      val = prev
    } else if ((ind %% 1000) == 0) {
      cat(ind, val, delta, prev, (prev - start), ' OK\n')
      
      if (prev == val) {
        cat('prev == val', ind, prev, val, delta, '\n')
        break
      }
    }
    
    delta = delta / 2
    ind = ind + 1:1
  }
  cat('Final:     ', ind, val, delta, prev, '\n')
  cat('Increment: ', (val - start))
}

find_max_double()