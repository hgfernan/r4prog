prev = i = 1L
two = 2L
one = 1L
count = 0L
factor = 2L
while ((! is.na(i))  && (count < 40L)) {
  prev <- i
  i <- i + factor;
  factor <- factor * 2L
  
  count <- count + 1L
}

if (is.na(i)) {
  i = prev  
}
cat("Final i:    ", i, class(i), log(i, 2), '\n')
cat("      prev: ", prev, "class ", class(prev), '\n')
cat("            ", "log2(prev) ", log(prev, 2), "log10(prev) ", log(prev, 10), '\n')
cat("--------------------\n")

ind <- 1L
i <- 2L
while ( (! is.na(i)) && (ind < 40L) ) {
  prev = i  
  i = i * 2L
  ind = ind + 1L;
  cat(ind, i) ; cat("\n")
}

if (is.na(i)) {
  i = prev  
}

half = i %/% 2L
count = one
while (half > 0L) {
  prev = i
  i = i + half
  
  if (is.na(i)) {
    cat(prev, "+", half, " == ", i, mode(i), "\n")
    i = prev
  }
  
  if ((count %% 5L) == 0) {
    cat(count, i, half, "\n")
  } 

  half = half %/% two
  count = count + one
}

cat("Final i:    ", i, class(i), log(i, 2), '\n')
cat("      prev: ", prev, "class ", class(prev), '\n')
cat("            ", "log2(prev) ", log(prev, 2), "log10(prev) ", log(prev, 10), '\n')

nprev = -prev
cat("Negative limit: ", nprev, "\n")
