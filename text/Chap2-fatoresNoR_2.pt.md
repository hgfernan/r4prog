---
output: pdf_document
---
# Fatores no `R` 2 -- Fatores ordenados

Além dos fatores básicos apresentados anteriormente, há também os fatores 
ordenados, ou `ordered`. Em termos práticos, não são tão diferentes, mas 
permitem algumas operações adicionais, que podem ser úteis em alguns contextos.

## Criaçao de fatores ordenados

Repetindo os passos iniciais de fatores convencionais, para comparação.

```R
incomes <- c(60, 49, 40, 61, 64, 60, 59, 54, 62, 69, 70, 42, 56,
             61, 61, 61, 58, 51, 48, 65, 49, 49, 41, 48, 52, 46,
             59, 46, 58, 43)
state <- c("tas", "sa",  "qld", "nsw", "nsw", "nt",  "wa",  "wa",
           "qld", "vic", "nsw", "vic", "qld", "qld", "sa",  "tas",
           "sa",  "nt",  "wa",  "vic", "qld", "nsw", "nsw", "wa",
           "sa",  "act", "nsw", "vic", "vic", "act")

( statef <- factor(state) )
```

Neste caso, a linha da criação de fatores ordenados traz uma novidade em relação
àquela de fatores convencionais


```Routput
> ( stateo <- ordered(state) )
 [1] tas sa  qld nsw nsw nt  wa  wa  qld vic nsw vic qld qld sa  tas sa  nt  wa  vic qld nsw nsw wa  sa 
[26] act nsw vic vic act
Levels: act < nsw < nt < qld < sa < tas < vic < wa
```

Ou seja: agora foi uma ordem entre os fatores. As cadeias de caracteres dos 
níveis continuam ordenadas por ordem alfabética, mas agora há também uma ordem 
dos fatores, expressa pelos sinais de menor `<` mostrados nos níveis.

Isto permite, por exemplo, estabelecer máximo e mínimo dos fatores. 

```Routput
> min(stateo)
[1] act
Levels: act < nsw < nt < qld < sa < tas < vic < wa
> max(stateo)
[1] wa
Levels: act < nsw < nt < qld < sa < tas < vic < wa
```

As informações adicionais continuam as mesmas dos fatores tradicionais: 


```Routput
> levels(stateo)
[1] "act" "nsw" "nt"  "qld" "sa"  "tas" "vic" "wa" 
> nlevels(stateo)
[1] 8
> summary(stateo)
act nsw  nt qld  sa tas vic  wa 
  2   6   2   5   4   2   5   4 
> 
> c(length(state), length(incomes), length(stateo))
[1] 30 30 30
> mode(stateo)
[1] "numeric"
```

Mas uma novidade é a classe dos fatores ordenados: 

```Routput
> class(stateo)
[1] "ordered" "factor" 
```

Isto indica que a classe dos fatores ordenados é **derivada** daquela dos 
fatores convencionais. Ou seja: ela usa os recursos definido para fatores 
convencionais e acrescenta a eles alguns outros. Ou os altera.

Veremos sobre programação orientada em `R` mais adiante neste curso.

Também são equivalentes as formas de converter o conteúdo para números ou para 
caracteres de fatores convencionais ou ordenados.

```Routput
> as.numeric(stateo)
 [1] 6 5 4 2 2 3 8 8 4 7 2 7 4 4 5 6 5 3 8 7 4 2 2 8 5 1 2 7 7 1
> as.character(stateo)
 [1] "tas" "sa"  "qld" "nsw" "nsw" "nt"  "wa"  "wa"  "qld" "vic" "nsw" "vic" "qld" "qld" "sa"  "tas" "sa" 
[18] "nt"  "wa"  "vic" "qld" "nsw" "nsw" "wa"  "sa"  "act" "nsw" "vic" "vic" "act"
```

Um recurso interessante para avaliar fatores é sua transformação em uma tabela, 
através de `table()`:

```Routput
> ( t <- table(stateo) )
stateo
act nsw  nt qld  sa tas vic  wa 
  2   6   2   5   4   2   5   4 
```

Apesar da aparência simples do resultado, isto permite criar gráficos de 
barras para visualizar a importância dos fatores. Entre muitas outras 
possibilidades. 

```Routput
> plot(table(stateo))
```

![Gráfico de barras do fator ordenado](img/stateo_barbplots.jpeg "Gráfico de barras do fator ordenado")

