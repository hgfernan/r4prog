---
output: pdf_document
---
# Fatores no `R` 3 -- Operações com fatores

Conforme comentado, o uso mais interessante de fatores, ordenados ou não, é em
estruturas maiores. No entanto, também é interessante verificar algumas 
operações que podem ser aplicados a fatores isoladamente.

## Troca de valores em fatores

1. Troca de valores na cadeia de caracteres base, com elementos pertencentes 
    ao conjunto de níveis;
    
2. Troca de valores fora do conjunto de níveis -- maiúsculas 

    => geração de NA;
    
3. Reparo

  stateo[is.na(stateo)] <- 'tas'

## Troca de níveis

```R
x <- factor(c("single","married","married","single"))
levels(x) <- c(levels(x), "widowed")    # add new level
```

## Exclusão de níveis não usados

*Ver `help(factor)`*

## Atribuição de rótulos

```R
aus.names <- character()
aus.names["ACT"] <- "Australian Capital Territory"
aus.names["NSW"] <- "New South Wales"
aus.names["NT"] <- "Northern Territory"
aus.names["QLD"] <- "Queensland"
aus.names["SA"] <- "South Australia"
aus.names["TAS"] <- "Tasmania"
aus.names["VIC"] <- "Victoria"
aus.names["WA"] <- "Western Australia"
stateo <- ordered(state, labels = aus.names) )
```

## Rótulos enumerados 

```R
stateo <- ordered(state, labels = "Aus") )
```

