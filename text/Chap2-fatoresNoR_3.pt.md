---
output: pdf_document
---
# Fatores no `R` 3 -- Fatores gerados sinteticamente

Até aqui vimos fatores gerados a partir de um conjunto de dados já existente. 
Agora veremos uma função que permite gerar um fator sem a necessidade de um 
conjunto de dados.

A complexidade dos padrões de ocorrência de níveis é limitada, mas justamente 
pode atender a situações onde se tenha esse tipo de padrões. 

Complexidades maiores podem ser geradas pela própria função `factor()`, como 
se verá no próximo vídeo.

## Os parâmetros da função `gl()`

`gl(n, k, length = n*k, labels = seq_len(n), ordered = FALSE)`

Onde 

* `n` é o número de níveis;

* `k` é o número de repetições;

* `length`, ou comprimento, descreve o número total de elementos. 

    Por omissão (ou *default*), o comprimento é `n * k `

* `labels` é um vetor de nomes para os valores. Por omissão, ele é apenas os
    índices dos números de níveis;
    
* `labels`, um vetor de rótulos para os níveis do fator. É opcional.

* `ordered`, um valor lógico que indica se o fator deverá ser ordenado ou
  não.

## Alguns exemplos

```R
## No labels
( f0 <- gl(2, 8) )

## First control, then treatment:
( f1 <- gl(2, 8, labels = c("Control", "Treat")) )

## 20 alternating 1s and 2s
( f2 <- gl(2, 1, 20, labels = c("Control", "Treat")) )

## alternating pairs of 1s and 2s
gl(2, 2, 20, labels = c("Control", "Treat")) )
```

## Classe dos resultados

Quando `ordered` não é fornecido, ou é fornecido, falso, a classe é `factor`.

Quando `ordered` é atribuído como `TRUE`, a classe é `ordered`.

