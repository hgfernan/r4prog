# Números complexos

## Primeira visão

`complex-01.R`

## Recursos de números complexos

`complex-02.R`

* Operações básicas com números complexos.
* Menção sobre imaturidade na implementação dos complexos.

## Exemplos de operações de números complexos

`complex-03.R`

* Variedades de números, em diferentes ângulos, com diferentes operações

## Impressão de números complexos

`complex-04.R`

* Exemplos do uso de `print()`, `format()` e `cat(), como nos exemplos
  anteriores;
  
* Comentário sobre facilidade e dificuldade da formatação padrão de complexos.


