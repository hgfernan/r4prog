---
output: pdf_document
---
# Capítulo 2 -- *Bytes* no R, parte 3

## Lição de casa para os próximos vídeos

1. Fazer a conversão de inteiro para `raw` e vice-versa;
  
2. Fazer a conversão de complexo para `raw` e vice-versa. A documentação é 
  dúbia em alguns pontos, mas há suporte nativo para números complexos.

3. Fazer a conversão de caracter para `raw` e vice-versa. Qual será a 
  maior dificuldade ?
  
