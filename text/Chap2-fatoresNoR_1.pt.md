---
output: pdf_document
---
# Fatores no `R` 1 -- primeira abordagem

O conceito de *factor*, ou **fator**, ficará mais claro apenas quando estiver associado a novos elementos, em itens à frente nessa série de vídeos, que é base do livro em desenvolvimento chamado **R para programar**.

Em poucas palavras, de modo simplificado, fatores permitem gerar subgrupos de um modo muito elegante e flexível, com alguns toques matemáticos.

Para que servem subgrupos ? Por exemplo, imagine uma cadeia nacional de supermercados com atuação em diversos estados, diversas cidades e diversos bairros. Através de subgrupos -- ou **fatores** -- será possível analisar os resultados dessas lojas em diferentes agrupamentos.

Aqui faremos uma abordagem das operações básicas de fatores: inicialmente de seu concceito mais básico. E depois de alguns elementos mais avançados, que mostrarão como é feita sua implementação.

## Visão geral de fatores em `R`

Agui seguimos parte da abordagem proposta pelo livro [An Introduction to R](https://cran.r-project.org/doc/manuals/R-intro.html), um dos manuais sobre `R` disponíveis no portal do [Projeto R](https://cran.r-project.org), onde são lançadas as versões oficiais do software.

Considere um conjunto de dados sobre rendimentos, ou *incomes*, de pesssoas em estados australianos. Naturalmente, haverá para cada pessoa haverá várias informações -- que no `R` muitas vezes são chamadas *data frames* -- literalmente molduras de dados. Mas neste momento conhecemos apenas vetores de dados. Por simplicidade, focaremos apenas nos rendimentos e nas siglas dos estados australianos. Então, seja nossa amostra de dados rendimentos na forma 

```R
incomes <- c(60, 49, 40, 61, 64, 60, 59, 54, 62, 69, 70, 42, 56,
             61, 61, 61, 58, 51, 48, 65, 49, 49, 41, 48, 52, 46,
             59, 46, 58, 43)
state <- c("tas", "sa",  "qld", "nsw", "nsw", "nt",  "wa",  "wa",
           "qld", "vic", "nsw", "vic", "qld", "qld", "sa",  "tas",
           "sa",  "nt",  "wa",  "vic", "qld", "nsw", "nsw", "wa",
           "sa",  "act", "nsw", "vic", "vic", "act")
```

Há alguns cuidados a serem tomados para que esta coleção de dados -- na forma como é apresentada -- faça sentido. Por exemplo, é preciso que o tamanho dos vetores seja igual; caso contrário haverá rendimentos não associados a um estado ou vice-versa.

Naturalmente, podemos checar essa informação através da função `length()`, impressas através de eco na console:

```R
c(length(state), length(incomes))
```

Uma das formas de organizar os dados é através de fatores:

```R
statef <- factor(state)
levels(statef)
summary(statef)
```

A primeira linha cria a variável `statef` que é do tipo fator, ou `factor`. A segunda linha mostra os elementos distintos dentro do fator -- que são chamados de **níveis**, ou `levels`. 

Finalmente, a terceira linha, `summary(statef)` apresenta estatísticas para os níveis. 

Para entender um pouco mais como é construído um fator, algumas informações a mais: 

```R 
c(length(state), length(incomes), length(statef))
mode(statef)
class(statef)
```

A primeira linha mostra que o fator `statef` tem o mesmo comprimento do vetor `state`. Que, por sua vez, também tem o mesmo comprimento.

A segunda linha, com `mode()`, mostra que, internamente, um fator é uma variável numérica.  A terceira linha mostra que `statef` pertence a uma classe especial, chamada `factor`. 

As duas linhas seguintes mostram que um fator tem duas faces -- pelo menos: 

```R 
as.numeric(statef)
as.character(statef)
```

Internamente, um fator tem informação numérica que associa seus estados ordenados alfabeticamente com sua posição no vetor. Por exemplo, na primeira posição está o estado `tas`, que é o sexto na ordenação alfabética.

Os recursos disponíveis para fatores em R são muitos, e é impressionante sua flexibilidade disponível no pacote `base` do R.  Além disso, há o pacote `forcats , com ainda mais recursos. Mas isso é tema para outros vídeos. 
