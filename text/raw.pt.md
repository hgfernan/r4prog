# Dados `raw`

## Primeira visão

`raw-01.R`

* Visão geral -- *bytes* em outros termos
* Coerção de caracteres, com informação sobre ASCII, UNICODE e UTF-8

## Recursos do tipo `raw`

`raw-02.R`

* Coerção de outros tipos de dados

## Entrada binária de dados `raw`

`raw-03.R`

## Impressão de dados `raw`

`raw-04.R`


